//
//  CitiesJsonFileReader.swift
//  WeatherMap
//
//  Created by Admin on 03.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import Foundation
import SwiftyJSON

class CitiesJsonFileReader{
    private var fileName : String = "cities"
    private var fileExtension : String = "json"
    
    
    func parceJsonFile() -> [CityWeatherDescription]! {
        
        guard let path = Bundle.main.url(forResource: fileName, withExtension: fileExtension) else{
            return nil
        }
        
        let data = try! Data(contentsOf: path)
        
        let jsonData = JSON(data: data, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
        
        guard let countries = jsonData.dictionary else{
            return nil
        }
        
        var result = [CityWeatherDescription]()
        for country in countries{
            let countryCode = country.key
            for city in country.value.array!{
                let cityDescription = CityWeatherDescription()
                cityDescription.id = city["_id"].intValue
                cityDescription.lat = city["coord"]["lat"].doubleValue
                cityDescription.lon = city["coord"]["lon"].doubleValue
                cityDescription.city = city["name"].description
                cityDescription.countryCode = countryCode
                result.append(cityDescription)
            }
        }
        
        return result
    }
}

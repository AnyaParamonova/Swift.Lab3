//
//  WeatherDetailedViewController.swift
//  WeatherMap
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit
import MapKit

class WeatherDetailedViewController: UIViewController {

    @IBOutlet weak var weatherMap: MKMapView!
    
    var cityWeatherDescription : CityWeatherDescription!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (cityWeatherDescription) != nil{
            let span = MKCoordinateSpanMake(0.1, 0.1)
            let location = CLLocationCoordinate2DMake(cityWeatherDescription.lat, cityWeatherDescription.lon)
            let region = MKCoordinateRegionMake(location, span)
            weatherMap.setRegion(region, animated: true)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = cityWeatherDescription.city + ": " + cityWeatherDescription.description
            annotation.subtitle = "Temperature: " + String(cityWeatherDescription.temperatureInCelcius) + "C, " + "Pressure: " + String(cityWeatherDescription.pressure)
            weatherMap.addAnnotation(annotation)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

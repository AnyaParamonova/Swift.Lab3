//
//  CityWeatherDescription.swift
//  WeatherMap
//
//  Created by Admin on 03.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import Foundation

class CityWeatherDescription{
    var id : Int = -1
    var city : String = ""
    var countryCode : String = ""
    var lon : Double = 0.0
    var lat : Double = 0.0
    
    var temperatureInCelcius : Double = 0.0
    var pressure : Double = 0.0
    var windSpeed : Double = 0.0
    var description : String = ""
}
